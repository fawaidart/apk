import React, { Component, useState } from "react";
import { Text, TouchableOpacity, View, StatusBar, TextInput, ScrollView, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Header from "../componen/Header";

class Home extends Component{
    constructor(props){
        super(props);
        this.state = {};
    }
    
    render(){
        
        return(
            <View style={{ flex: 1, backgroundColor: '#f4f4f4' }} >
                
                <View style={{ marginHorizontal: 20, marginBottom: 20, marginTop: 20 }}>
                    <Text style={{ fontSize: 28, fontWeight: 'bold', color: '#212121' }}>
                        resep<Text style={{ color: '#4169e1' }}>ibu</Text>
                    </Text>
                </View>
                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginHorizontal: 50, marginBottom: 20,
                        }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#212121' }}>
                            Menu
                        </Text>
                    </View>
                <ScrollView>
                     <Header/>
                </ScrollView>
                <View
                    style={{
                        flexDirection: 'row',
                        paddingTop: 5,
                        backgroundColor: '#FFFFFF',
                    }}>
                    <TouchableOpacity
                        style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}
                    // onPress={() => navigation.navigate('Home')}
                    >
                        <Icon name="home" size={25} color="#0082F7" />
                        <Text style={{ color: '#0082F7' }}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Details')}
                        style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}
                    // onPress={() => navigation.navigate('Kategori')}
                    >
                        <Icon name="file-tray-full" size={25} color="#bdbdbd" />
                        <Text style={{ color: '#bdbdbd' }}>Details</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('User')}
                        style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                        <Icon name="person" size={25} color="#bdbdbd" />
                        <Text style={{ color: '#bdbdbd' }}>User</Text>
                    </TouchableOpacity>
                </View>
            </View>
            
        )
    }

}
export default Home;