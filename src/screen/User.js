import React, { Component } from "react";
import { Text, TouchableOpacity, View, ScrollView, StatusBar } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import HeaderUS from "../componen/HeaderUS";
class User extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#f4f4f4' }}>
              
                    <View>
                        <StatusBar barStyle="dark-content" backgroundColor={'#f4f4f4'} />
                        <Text style={{ color: '#212121' }}>Hello</Text>
                        <Text style={{ fontSize: 22, fontWeight: 'bold', color: '#212121' }}>
                            fawaid 👋
                        </Text>
                        
                    </View>
                 <HeaderUS/>
              
                <View
                    style={{
                        flexDirection: 'row',
                        paddingTop: 5,
                        backgroundColor: '#FFFFFF',
                    }}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Home')}
                        style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}
                    // onPress={() => navigation.navigate('Home')}
                    >
                        <Icon name="home" size={25} color="#bdbdbd" />
                        <Text style={{ color: '#bdbdbd' }}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Details')}
                        style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}
                    // onPress={() => navigation.navigate('Kategori')}
                    >
                        <Icon name="file-tray-full" size={25} color="#bdbdbd" />
                        <Text style={{ color: '#bdbdbd' }}>Details</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('User')}
                        style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                        <Icon name="person" size={25} color="#0082F7" />
                        <Text style={{ color: '#0082F7' }}>User</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}
export default User;