import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    TextInput,
    Image,
    FlatList,
    ScrollView,
    ImageBackground,
} from 'react-native';
const HeaderDT = () => {

    const [pencarian, setPencarian] = useState('');


    const [artikel, setArtikel] = useState([
        {
            judul: 'Ayam Goreng Tepung ala Korea',
            deskripsi: 'inilah resep rahasia agar anda tetap sehat di umur 60an',
            image: require('../images/12.jpg'),
        },
        // {
        //     judul: 'Ayam Goreng Tepung Spesial',
        //     deskripsi: 'inilah resep rahasia agar anda tetap sehat di umur 60an',
        //     image: require('../images/2.jpg'),
        // },
        // {
        //     judul: 'Ayam Goreng Tepung Tanpa Telur',
        //     deskripsi: 'inilah resep rahasia agar anda tetap sehat di umur 60an',
        //     image: require('../images/4.jpg'),
        // },
    ]);

    return (
        <View style={{ flex: 1, backgroundColor: '#f4f4f4' }}>
           
                <View>
                    <FlatList
                        data={artikel}
                        style={{ marginBottom: 20 }}
                        renderItem={({ item }) => (
                            <TouchableOpacity
                                // onPress={() => this.props.navigation.navigate('Details')}
                                style={{
                                    marginRight: 10,
                                    backgroundColor: '#FFFFFF',
                                    borderRadius: 10,
                                    elevation: 3,
                                    marginBottom: 10,
                                    marginTop: 10,
                                    paddingBottom: 20,
                                }}>
                                <View
                                    style={{
                                        height: 200,
                                        marginBottom: 10,
                                        borderTopRightRadius: 10,
                                        borderTopLeftRadius: 10,
                                    }}>
                                    <ImageBackground
                                        source={item.image}
                                        style={{
                                            flex: 1,
                                            borderTopRightRadius: 10,
                                            borderTopLeftRadius: 10,
                                        }}
                                        imageStyle={{
                                            borderTopRightRadius: 10,
                                            borderTopLeftRadius: 10,
                                        }}
                                    />
                                </View>
                                <View>
                                    <Image
                                        source={item.image}
                                        style={{
                                            width: 40,
                                            height: 40,
                                            borderRadius: 20,
                                            borderColor: '#FFFFFF',
                                            borderWidth: 2,
                                            marginLeft: 10,
                                            marginTop: 5,
                                        }}
                                    />
                                    <Text
                                        style={{
                                            fontWeight: 'bold',
                                            fontSize: 18,
                                            marginHorizontal: 10,
                                            marginTop: 5,
                                        }}>
                                        {item.judul}
                                    </Text>
                                    <Text
                                        style={{
                                            marginHorizontal: 10,
                                        }}>
                                        <Text>Bahan-bahan:</Text>  
                                    </Text>
                                    <Text style={{
                                        marginHorizontal: 10,
                                    }}>- 500 gram ayam potong (marinate minimal 30 menit pakai perasan lemon, merica dan garam)
                                        - Irisan daun bawang
                                        - Biji wijen secukupnya
                                        Bumbu pelapis (aduk rata):
                                        - 8 sdm tepung terigu
                                        - 2 sdm tepung beras
                                        - 2 sdm tepung maizena
                                        - Merica dan garam secukupnya
                                        Bumbu saus pedas:
                                        - 1 sdt bawang putih bubuk
                                        - 1/2 sdt jahe bubuk
                                        - 2 sdm gochujang
                                        - 2 sdm gochugaru
                                        - 1 sdm madu
                                        - 1 sdm kecap asin
                                        - 1 sdt merica
                                        - 1 sdt air lemon
                                       
                                    </Text>

                                    <Text
                                        style={{
                                            marginHorizontal: 10,
                                        }}>
                                        <Text> Cara memasak:</Text>
                                    </Text>

                                    <Text
                                        style={{
                                            marginHorizontal: 10,
                                        }}>
                                        <Text> 1. Aduk rata bumbu pelapis, bagi 2, satu biarkan kering, satu lagi tuang air matang sedikit.
                                            
                                            
                                            </Text>
                                    </Text>
                                    <Text
                                        style={{
                                            marginHorizontal: 10,
                                        }}>
                                        <Text> 2. Celupkan ayam ke adonan tepung basah, lalu lumuri adonan tepung kering.</Text>
                                    </Text>
                                    <Text
                                        style={{
                                            marginHorizontal: 10,
                                        }}>
                                        <Text> 3. Goreng ayam, tiriskan.</Text>
                                    </Text>
                                    <Text
                                        style={{
                                            marginHorizontal: 10,
                                        }}>
                                        <Text>4. Aduk bumbu saus pedas, panaskan sebentar. Masukkan ayam, aduk rata. Taburi daun bawang dan biji wijen.</Text>
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        )}
                    />
                </View>
           


        </View>
    );
};

export default HeaderDT;
