import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    TextInput,
    Image,
    FlatList,
    ScrollView,
    ImageBackground,
} from 'react-native';
const Header = () => {
    
    const [pencarian, setPencarian] = useState('');
    

    const [artikel, setArtikel] = useState([
        {
            judul: 'Ayam Goreng Tepung ala Korea',
            
            image: require('../images/12.jpg'),
        },
        {
            judul: 'Ayam Goreng Tepung Spesial',
            
            image: require('../images/2.jpg'),
        },
        {
            judul: 'Ayam Goreng Tepung Tanpa Telur',
           
            image: require('../images/4.jpg'),
        },
    ]);

    return (
        <View style={{ flex: 1, backgroundColor: '#f4f4f4' }}>
            
                <View>
                    <FlatList
                        data={artikel}
                        style={{ marginBottom: 20 }}
                        renderItem={({ item }) => (
                            <TouchableOpacity
                                // onPress={() => this.props.navigation.navigate('Details')}
                                style={{
                                    marginRight: 10,
                                    backgroundColor: '#FFFFFF',
                                    borderRadius: 10,
                                    elevation: 3,
                                    marginBottom: 10,
                                    marginTop: 10,
                                    paddingBottom: 20,
                                }}>
                                <View
                                    style={{
                                        height: 200,
                                        marginBottom: 10,
                                        borderTopRightRadius: 10,
                                        borderTopLeftRadius: 10,
                                    }}>
                                    <ImageBackground
                                        source={item.image}
                                        style={{
                                            flex: 1,
                                            borderTopRightRadius: 10,
                                            borderTopLeftRadius: 10,
                                        }}
                                        imageStyle={{
                                            borderTopRightRadius: 10,
                                            borderTopLeftRadius: 10,
                                        }}
                                    />
                                </View>
                                <View>
                                    <Image
                                        source={item.image}
                                        style={{
                                            width: 40,
                                            height: 40,
                                            borderRadius: 20,
                                            borderColor: '#FFFFFF',
                                            borderWidth: 2,
                                            marginLeft: 10,
                                            marginTop: 5,
                                        }}
                                    />
                                    <Text
                                        style={{
                                            fontWeight: 'bold',
                                            fontSize: 18,
                                            marginHorizontal: 10,
                                            marginTop: 5,
                                        }}>
                                        {item.judul}
                                    </Text>
                                    <Text
                                        style={{
                                            marginHorizontal: 10,
                                        }}>
                                        {item.deskripsi}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        )}
                    />
                </View>
          

            
        </View>
    );
};

export default Header;
